# frozen_string_literal: true

class ProcessPaymentWorker
  include Sidekiq::Worker

  def perform(id)
    transaction = Transaction.find(id)
    ProcessPaymentService.call(transaction: transaction)
  end
end