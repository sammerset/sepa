# frozen_string_literal: true

class SepaIdentifierValidator < Apipie::Validator::BaseValidator
  REGEX = /\A[a-zA-Z]{2,2}[0-9]{2,2}([A-Za-z0-9]|[\+|\?|\/|\-|\:|\(|\)|\.|,|']){3,3}([A-Za-z0-9]|[\+|\?|\/|\-|:|\(|\)|\.|,|']){1,28}\z/

  def initialize(param_description, argument)
    super(param_description)
    @type = argument
  end

  def validate(value)
    return false if value.nil?

    !!(value =~ REGEX) && (value[0..1].match(/DE/i) ? value.length.eql?(18) : true )
  end

  def self.build(param_description, argument, options, block)
    self.new(param_description, argument)
  end

  def description
    "Must be #{@type}."
  end
end