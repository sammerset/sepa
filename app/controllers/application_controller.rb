# frozen_string_literal: true

class ApplicationController < ActionController::API
  include Devise::Controllers::Helpers
  include DeviseTokenAuth::Concerns::SetUserByToken

  before_action :authenticate_user!

  protected

  rescue_from ArgumentError do |e|
    render json: {
             'ErrorType' => 'Validation Error',
             'message' => e.message
           }, code: :bad_request
  end

  private

  def current_customer
    current_user&.customer
  end
end