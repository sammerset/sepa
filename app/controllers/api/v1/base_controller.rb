# frozen_string_literal: true

module Api::V1
  class BaseController < ApplicationController
    resource_description do
      api_version '1'
    end
  end
end
