# frozen_string_literal: true

module Api::V1
  class TransactionsController < BaseController
    def_param_group :transaction do
      param :debtor, Hash, required: true do
        param_group :account
      end
      param :creditor, Hash, required: true do
        param_group :account
      end
      param :attributes, Hash, required: true do
        param_group :attributes
      end
    end

    # amount in cents
    def_param_group :attributes do
      param :amount,   /\d+/, 'Amount of transaction', required: true
      param :currency, Transaction::CURRENCIES, 'Transaction currency', required: true
    end

    # account info
    def_param_group :account do
      param :iban, /\A[A-Z]{2,2}[0-9]{2,2}[a-zA-Z0-9]{1,30}\z/, desc: 'IBAN', required: true
      param :bic, /\A[A-Z]{6,6}[A-Z2-9][A-NP-Z0-9]([A-Z0-9]{3,3}){0,1}\z/, desc: 'BIC'
      param :sepa_id, lambda { |val|
        if val =~ /\A[a-zA-Z]{2,2}[0-9]{2,2}([A-Za-z0-9]|[\+|\?|\/|\-|\:|\(|\)|\.|,|']){3,3}([A-Za-z0-9]|[\+|\?|\/|\-|:|\(|\)|\.|,|']){1,28}\z/
          if val[0..1].match(/DE/i)
            val.length.eql?(18) ? true : 'Sepa identifier DE domain invalid.'
          else
            true
          end
        else
          'Sepa identifier invalid.'
        end
      }, desc: 'Sepa identifier', required: true
      param :name,      String, 'Full name', required: true
      param :address,   String, 'Address 1 line', required: true
      param :address_2, String, 'Address 2 line', required: false
      param :postcode,  String, 'Postcode', required: true
      param :town,      String, 'Town', required: true
      param :country,   String, 'Country', required: true
      param :contact,   String, 'Contact', required: true
      param :phone,     String, 'Phone', required: true
      param :email,     String, 'Email', required: true
    end

    api :GET, '/transactions', 'List of all transactions'
    def index
      render json: current_customer.transactions, status: 200
    end

    api :GET, '/transactions/:id', 'Get transaction'
    param :id, /\d+/
    def show
      render json: current_customer.transactions.find(params[:id]), status: 200
    end

    api :POST, '/transactions', 'Init transaction'
    param_group :transaction
    def create
      transaction = Transaction.new(
        metadata: params.slice(:debtor, :creditor, :attributes)
      )

      if transaction.save
        ProcessPaymentWorker.perform_async(transaction.id)

        render json: { uuid: transaction.id }, status: 201
      else
        render json: { errors: transaction.errors.full_messages }, status: 422
      end
    end
  end
end