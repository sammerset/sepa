# frozen_string_literal: true

class Customer < ActiveRecord::Base
  validates :email, :addr, :postcode, :town, :country,
            :contact, :phone, presence: true

  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :name, presence: true, uniqueness: true

  has_many :transactions
  has_one :mandate
  belongs_to :user
end