# frozen_string_literal: true

class Mandate < ActiveRecord::Base
  SEQUENCE_TYPES = %w(FRST OOFF RCUR FNAL)

  belongs_to :customer

  validates :sig_date, presence: true
  validates :name,
    format: { with: /\A([A-Za-z0-9]|[\+|\?|\/|\-|\:|\(|\)|\.|\,|\']){1,35}\z/ },
    presence: true, uniqueness: true

  validates_inclusion_of :sequence_type, in: SEQUENCE_TYPES
end