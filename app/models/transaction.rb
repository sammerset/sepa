# frozen_string_literal: true

class Transaction < ActiveRecord::Base
  include AASM

  CURRENCIES = %w[ EUR USD ].freeze

  belongs_to :customer

  aasm do
    state :pending, initial: true
    state :processing, :finished, :rejected

    event :process do
      transitions from: :pending, to: :processing
    end

    event :finish do
      transitions from: :processing, to: :finished
    end

    event :reject do
      transitions from: [:pending, :processing], to: :rejected
    end
  end

  def debitor_sepa_identifier
    debitor_meta[:sepa_identifier]
  end

  def creditor_sepa_identifier
    creditor_meta[:sepa_identifier]
  end

  def amount
    transaction_attrs[:amount]
  end

  def currency
    transaction_attrs[:amount]
  end

  def debtor
    debtor_meta
  end

  def creditor
    creditor_meta
  end

  private

  def meta
    metadata.with_indifferent_access
  end

  def transaction_attrs
    meta[:attributes]
  end

  def debtor_meta
    meta[:debtor]
  end

  def creditor_meta
    meta[:creditor]
  end
end