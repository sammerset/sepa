# frozen_string_literal: true

class ProcessPaymentService
  extend SmartInit

  is_callable

  initialize_with :transaction

  def call
    ActiveRecord::Base.transaction do
      @xml_order = CreateOrderService.call(transaction: @transaction)

      process_payment!
      finish_payment!
    end
  rescue => e
    @transaction.error = e.message
    @transaction.reject!
  end

  private

  def process_payment!
    bank_transaction, order_id = ebics_client.CDD(@xml_order)

    @transaction.assign_attributes(
      xml_order: @xml_order,
      bank_transaction: bank_transaction,
      order_id: order_id
    )

    @transaction.process!
  end

  def ebics_client
    @ebics_client ||= Epics::Client.new(
      ebics_config.key,
      ebics_config.password,
      ebics_config.bank_url,
      ebics_config.host_id,
      ebics_config.user_id,
      ebics_config.partner_id
    )
  end

  def ebics_config
    Rails.application.secrets.ebics
  end
end