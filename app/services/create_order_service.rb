# frozen_string_literal: true

class CreateOrderService
  extend SmartInit

  BANK_ACCOUNT_KEYS = %i[
    name address address_2
    postcode town country
    contact phone email
  ].freeze

  is_callable

  initialize_with :transaction

  def call
    create_order
  end

  private

  def create_order
    direct_debit = Sepa::DirectDebitOrder::DirectDebit.new(
      debtor,
      debtor_bank_account,
      @transaction.id,
      @transaction.amount.to_i,
      @transaction.currency,
      mandate
    )

    payment = Sepa::DirectDebitOrder::CreditorPayment.new(
      creditor,
      creditor_bank_account,
      @transaction.id,
      @transaction.created_at.to_date,
      sepa_identifier,
      [direct_debit]
    )
 
    order = Sepa::DirectDebitOrder::Order.new(
      @transaction.id,
      initiator,
      [payment]
    )

    order.to_xml(pain_008_001_version: '04')
  end

  def mandate
    @mandate ||= begin
      customer_mandate = @transaction.customer.mandate

      Sepa::DirectDebitOrder::MandateInformation.new(
        customer_mandate.name,
        customer_mandate.sig_date,
        customer_mandate.sequence_type
      )
    end
  end

  def debtor
    @debtor ||= Sepa::DirectDebitOrder::Party.new(
      *account_params(:debtor_meta)
    )
  end

  def creditor
    @creditor ||= Sepa::DirectDebitOrder::Party.new(
      *account_params(:creditor_meta)
    )
  end

  def initiator
    @creditor ||= Sepa::DirectDebitOrder::Party.new(
      *account_params(:customer)
    )
  end

  def debtor_bank_account
    @debtor_bank_account ||= Sepa::DirectDebitOrder::BankAccount.new(
      @transaction.debtor[:iban],
      @transaction.debtor[:bic]
    )
  end

  def creditor_bank_account
    @creditor_bank_account ||= Sepa::DirectDebitOrder::BankAccount.new(
      @transaction.creditor[:iban],
      @transaction.creditor[:bic]
    )
  end

  def account_params(method)
    meta = @transaction.send(method)

    BANK_ACCOUNT_KEYS.map do |key|
      meta.fetch(key)
    end
  end

  def sepa_identifier
    Sepa::DirectDebitOrder::PrivateSepaIdentifier.new(
      @transaction.creditor[:sepa_identifier]
    )
  end
end