# frozen_string_literal: true

class CreateCustomers < ActiveRecord::Migration[6.1]
  def change
    create_table :customers do |t|
      t.string :name, null: false
      t.string :email, null: false
      t.string :addr
      t.string :addr_2
      t.string :postcode, null: false
      t.string :town, null: false
      t.string :country, null: false
      t.string :contact, null: false
      t.string :phone, null: false

      t.timestamps
    end

    add_reference :customer, :user, index: true
  end
end