# frozen_string_literal: true

class CreateTransactions < ActiveRecord::Migration[6.1]
  def change
    create_table :transactions do |t|
      t.string :aasm_state, default: :pending
      t.text :xml_order
      t.string :bank_transaction
      t.string :order_id
      t.text :error
      t.json :metadata

      t.timestamps
    end

    add_reference :transactions, :customer, index: true
  end
end