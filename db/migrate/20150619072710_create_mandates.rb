# frozen_string_literal: true

class CreateMandates < ActiveRecord::Migration[6.1]
  def change
    create_table :mandates do |t|
      t.string :name
      t.string :sequence_type
      t.datetime :sig_date

      t.timestamps
    end

    add_reference :mandates, :customer, index: true
  end
end