# frozen_string_literal: true

Demo::Application.routes.draw do
  mount_devise_token_auth_for 'User', at: 'auth'

  apipie

  scope module: [:api, :v1], path: '/' do
    resources :transactions, only: %i[index show create]
  end
end