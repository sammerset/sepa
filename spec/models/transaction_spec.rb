# frozen_string_literal: true

require 'rails_helper'

describe Transaction do
  let(:transaction) { build(:transaction) }

  it '#create' do
    expect { transaction.save }.to change { Transaction.count }.by(1)
  end
end