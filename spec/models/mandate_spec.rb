# frozen_string_literal: true

require 'rails_helper'

describe Mandate do
  let(:mandate) { build(:mandate) }

  it { should validate_presence_of(:name).with_message('can\'t be blank') }
  it { should validate_presence_of(:sig_date).with_message('can\'t be blank') }
  it { should validate_presence_of(:sequence_type).with_message('is not included in the list') }

  it '#create' do
    expect { mandate.save }.to change { Mandate.count }.by(1)
  end
end