# frozen_string_literal: true

require 'rails_helper'

describe Customer do
  let(:customer) { build(:customer) }

  it { should validate_presence_of(:name).with_message('can\'t be blank') }
  it { should validate_presence_of(:email).with_message('can\'t be blank') }
  it { should validate_presence_of(:addr).with_message('can\'t be blank') }
  it { should validate_presence_of(:postcode).with_message('can\'t be blank') }
  it { should validate_presence_of(:town).with_message('can\'t be blank') }
  it { should validate_presence_of(:country).with_message('can\'t be blank') }
  it { should validate_presence_of(:contact).with_message('can\'t be blank') }
  it { should validate_presence_of(:phone).with_message('can\'t be blank') }

  it '#create' do
    expect { customer.save }.to change { Customer.count }.by(1)
  end
end