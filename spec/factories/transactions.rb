# frozen_string_literal: true

FactoryBot.define do
  factory :transaction, class: Transaction do
    metadata {
      {
        debtor: {
          iban: 'DE87200500001234567890',
          sepa_id: 'DE98ZZZ09999999999',
          name: 'Full name',
          address: 'Address 1 line',
          address_2: 'Address 2 line',
          postcode: 'Postcode',
          town: 'Town',
          country: 'Country',
          contact: 'Contact',
          phone: '+380961111111',
          email: 'debitor@email.com'
        },
        creditor: {
          iban: 'AU87200500001234567890',
          sepa_id: 'NL97ZZZ123456780001',
          name: 'Full name',
          address: 'Address 1 line',
          address_2: 'Address 2 line',
          postcode: 'Postcode',
          town: 'Town',
          country: 'Country',
          contact: 'Contact',
          phone: '+380962222222',
          email: 'creditor@email.com'
        },
        attributes: {
          amount: '100',
          currency: 'EUR'
        }
      }
    }
    customer
  end
end