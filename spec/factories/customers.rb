# frozen_string_literal: true

FactoryBot.define do
  factory :customer, class: Customer do
    user
    mandate
    sequence(:name) { |n| "Customer #{n}" }
    sequence(:email) { |n| "customer_#{n}@email.com" }
    addr { 'Addr 1' }
    postcode { '08002' }
    town { 'Town' }
    country { 'UA' }
    contact { 'Contact' }
    phone { '+380673003030' }
  end
end