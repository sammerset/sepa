# frozen_string_literal: true

FactoryBot.define do
  factory :mandate, class: Mandate do
    sequence(:name) { |n| "K-02-2011-1234#{n}" }
    sig_date { Date.current }
    sequence_type { 'FRST' }
  end
end