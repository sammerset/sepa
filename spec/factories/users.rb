# frozen_string_literal: true

FactoryBot.define do
  factory :user, class: User do
    sequence(:email) { |n| "customer_api_user#{n}@email.com" }
    password { 'password' }
    password_confirmation { 'password' }
  end
end