# frozen_string_literal: true

require 'rails_helper'

describe Api::V1::BaseController::TransactionsController, type: :controller do
  let(:user) { create(:user, customer: customer) }
  let(:customer) { create(:customer) }
  let!(:transaction) { create(:transaction, customer: customer) }

  before do
    allow(controller).to receive(:current_user).and_return(user)
  end

  describe 'GET /transactions' do
    it 'list of transactions' do
      get :index

      expect(response.body).to eq([transaction].to_json)
      expect(response.status).to eq(200)
    end
  end

  describe 'GET /transactions/:id' do
    it 'get transaction' do
      get :show, params: { id: transaction.id }

      expect(response.body).to eq(transaction.to_json)
      expect(response.status).to eq(200)
    end
  end

  describe 'POST /transactions' do
    let(:transaction_params) do
      {
        debtor: {
          iban: 'DE87200500001234567890',
          sepa_id: 'DE98ZZZ09999999999',
          name: 'Full name',
          address: 'Address 1 line',
          address_2: 'Address 2 line',
          postcode: 'Postcode',
          town: 'Town',
          country: 'Country',
          contact: 'Contact',
          phone: '+380961111111',
          email: 'debitor@email.com'
        },
        creditor: {
          iban: 'AU87200500001234567890',
          sepa_id: 'NL97ZZZ123456780001',
          name: 'Full name',
          address: 'Address 1 line',
          address_2: 'Address 2 line',
          postcode: 'Postcode',
          town: 'Town',
          country: 'Country',
          contact: 'Contact',
          phone: '+380962222222',
          email: 'creditor@email.com'
        },
        attributes: {
          amount: '100',
          currency: 'EUR'
        }
      }
    end
    let(:result) do
      { uuid: Transaction.last.id }.to_json
    end

    it 'init transaction' do
      post :create, params: transaction_params

      expect(response.body).to eq(result)
      expect(response.status).to eq(201)
      expect({aa: 11, bb: 55}).to include({bb: 55})
      expect(Transaction.last.metadata).to include(transaction_params.deep_stringify_keys)
    end
  end
end