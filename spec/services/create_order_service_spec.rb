# frozen_string_literal: true

require 'rails_helper'

describe CreateOrderService do
  let(:transaction) { create(:transaction) }
  let(:subject) { described_class.call(transaction: transaction) }

  before do
    
  end

  it '#call' do
    Timecop.freeze(Time.local(1990)) do
      expect(subject).to eq(file_fixture('sepa_order.xml').read) 
    end
  end
end